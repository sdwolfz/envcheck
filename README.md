# EnvCheck

Validate the presence of environment variables.

## Development

### Prerequisites

| Name          | Link                                    |
|---------------|-----------------------------------------|
| `bash`        | https://www.gnu.org/software/bash/      |
| `docker`      | https://docs.docker.com/engine/install/ |
| `docker-here` | https://gitlab.com/sdwolfz/docker-here  |
| `make`        | https://www.gnu.org/software/make/      |

Build the docker image with:

```sh
make build
```

Start a shell:

```sh
make shell
```

### Binary

Built by:

```sh
make compile
```

Run:

```sh
make run
```

### Config

Create a `.envcheck.yml` file with:

```yml
---

default:
  required:
    - PATH
    - PATHH
  warning:
    - HOME
    - HOMEA
  unwanted:
    - YOOOO
```
