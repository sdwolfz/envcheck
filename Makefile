.DEFAULT_GOAL = help

.PHONY: help
help:
	@echo 'Help'

.PHONY: build
build:
	@docker build --pull -t envcheck .

.PHONY: shell
shell:
	@docker-here envcheck sh

.PHONY: compile
compile:
	@shards build --static

.PHONY: run
run:
	@./bin/envcheck

.PHONY: test
test:
	@cd test/fixtures && ../../bin/envcheck
