require "yaml"

def extract_names(yaml, key)
  collection = yaml[key]?
  if collection
    collection = collection.as_a?

    unless collection
      puts "ERROR: key `#{key}` is not an Array!"

      exit 1
    end

    result = [] of String
    collection.each do |element|
      value = element.as_s?

      unless value
        puts "ERROR: value `#{element}` is not a String!"

        exit 1
      end

      result << value
    end

    result
  end
end

def handle_required(required : Array(String))
  failed = false
  required.each do |element|
    unless ENV.fetch(element, nil)
      puts "ERROR: Could not find required env variable: #{element}"

      failed = true
    end
  end

  exit 1 if failed
end

def handle_warning(warning : Array(String))
  warning.each do |element|
    unless ENV.fetch(element, nil)
      puts "WARNING: Could not find optional env variable: #{element}"
    end
  end
end

def handle_unwanted(unwanted : Array(String))
  failed = false
  unwanted.each do |element|
    if ENV.fetch(element, nil)
      puts "ERROR: Found unwanted env variable: #{element}"

      failed = true
    end
  end

  exit 1 if failed
end

yaml = File.read("./.envcheck.yml") rescue nil

if yaml
  config = YAML.parse(yaml)

  default = config["default"]?
  unless default
    puts "ERROR: key `default` not found in config file!"

    exit 1
  end

  required = extract_names(default, "required")
  handle_required(required) if required

  warning = extract_names(default, "warning")
  handle_warning(warning) if warning

  unwanted = extract_names(default, "unwanted")
  handle_unwanted(unwanted) if unwanted
else
  puts "ERROR: .envcheck.yml not found in cwd!"

  exit 1
end
