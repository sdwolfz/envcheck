FROM crystallang/crystal:0.36.1-alpine

RUN set -ex        && \
    mkdir -p /work

WORKDIR /work

RUN set -ex                       && \
    apk add --update --no-cache \
      make                      \
      yaml-dev                  \
      yaml-static
